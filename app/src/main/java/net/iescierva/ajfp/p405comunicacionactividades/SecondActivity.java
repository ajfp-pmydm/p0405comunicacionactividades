package net.iescierva.ajfp.p405comunicacionactividades;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    Button b, b2, botonFecha;
    TextView v2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);
        b = (Button) findViewById(R.id.aceptar);
        b2 = (Button) findViewById(R.id.rechazar);
        v2 = (TextView) findViewById(R.id.saludo);

        Bundle recogida = getIntent().getExtras();
        String s = recogida.getString("usuario");
        v2.setText("Hola " + s + ", ¿Aceptas las condiciones?");

        View.OnClickListener escuchador = v -> {
            Button boton_pulsado = (Button) v; // --> asi obtenemos el boton pulsado
            String s1 = boton_pulsado.getText().toString();

            Intent i;

            if (s1.equals("ACEPTAR")) {
                i = new Intent();
                i.putExtra("opcion", "Aceptado");
                setResult(RESULT_OK, i);
                finish();
            } else {
                i = new Intent();
                i.putExtra("opcion", "Rechazado");
                setResult(RESULT_OK, i);
                finish();
            }

        };

        b.setOnClickListener(escuchador);
        b2.setOnClickListener(escuchador);

        botonFecha = findViewById(R.id.boton_ver_fecha);
        botonFecha.setOnClickListener(this::lanzarVerFecha);

    }

    private void lanzarVerFecha(View view) {
        Intent i = new Intent(this, VerFechaActivity.class);
        i.putExtra("fecha", java.util.Calendar.getInstance().getTime().toString());
        startActivity(i);
    }
}