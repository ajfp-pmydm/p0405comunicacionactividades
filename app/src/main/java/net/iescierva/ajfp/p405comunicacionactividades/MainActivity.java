package net.iescierva.ajfp.p405comunicacionactividades;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    Button b, botonFecha;
    EditText v2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b=findViewById(R.id.verificar);
        b.setOnClickListener(v -> {
            v2=findViewById(R.id.edit);
            String s = v2.getText().toString();
            Intent i = new Intent(this,SecondActivity.class);
            i.putExtra("usuario", s);
            startActivityForResult(i, 1234);
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        TextView v = findViewById(R.id.resultado);

        if(requestCode==1234 && resultCode==RESULT_OK){
            String s = data.getStringExtra("opcion");
            v.setText("Resultado: "+s);
        }
    }
}