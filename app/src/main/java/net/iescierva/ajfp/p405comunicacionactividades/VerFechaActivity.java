package net.iescierva.ajfp.p405comunicacionactividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class VerFechaActivity extends AppCompatActivity {
TextView cajaFecha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_fecha);
        Bundle bundle = getIntent().getExtras();
        String fecha = bundle.getString("fecha");
        cajaFecha = findViewById(R.id.textFecha);
        cajaFecha.setText("FECHA: " + fecha);
    }
}